set -euo pipefail

# Update apt, install dependencies and what-not.
apt-get update  -qqy \
&&  apt-get upgrade -qqy \
&&  apt-get install -qqy curl tar git bash-completion \
&& rm -rf /var/cache/apt/*

adduser --system --no-create-home user

temp_dir=$(mktemp -d)
cd $temp_dir; trap 'exit_code=$?; rm -rf $temp_dir; exit $exit_code;' INT ERR EXIT

install_dir=/usr/local/bin
function install-bin(){ install -m755 "$temp_dir/$1" "$install_dir/"; }

set -o verbose
mkdir -p $install_dir

dasel_version=1.15.0
curl -sLo dasel https://github.com/TomWright/dasel/releases/download/v${dasel_version}/dasel_linux_amd64
install-bin dasel

doctl_version=1.61.0
curl -sL "https://github.com/digitalocean/doctl/releases/download/v${doctl_version}/doctl-${doctl_version}-linux-amd64.tar.gz" | tar -xz
install-bin doctl

hugo_version=0.82.0
curl -sL "https://github.com/gohugoio/hugo/releases/download/v${hugo_version}/hugo_extended_${hugo_version}_Linux-64bit.tar.gz" | tar -xz
install-bin hugo

mdbook_version=0.4.8
curl -sL "https://github.com/rust-lang/mdBook/releases/download/v${mdbook_version}/mdbook-v${mdbook_version}-x86_64-unknown-linux-gnu.tar.gz" | tar -xz
install-bin mdbook

zola_version=0.13.0
curl -sL "https://github.com/getzola/zola/releases/download/v${zola_version}/zola-v${zola_version}-x86_64-unknown-linux-gnu.tar.gz" | tar -xz
install-bin zola
