set -euo pipefail; cd "$(dirname $0)/.."
declare -a docker_build=( docker build --pull )

BUILD_DATE=$(date +%Y-%m-%d) BUILD_TIME=$(date +%H:%M:%S)
docker_build+=( --label="build.date=${BUILD_DATE}" --label="build.time=${BUILD_TIME}" )

GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
docker_build+=( --label="build.branch=${GIT_BRANCH}" )

# tags & caching from previous versions of each tag.
IMAGE_PATH="${CONTAINER_REGISTRY:-docker.pkg.github.com}/${GITHUB_REPOSITORY:-jakelogemann/my}/${1}"
docker_build+=( --cache-from="${IMAGE_PATH}:${GIT_BRANCH}" --tag="${IMAGE_PATH}:${GIT_BRANCH}" )
docker_build+=( --cache-from="${IMAGE_PATH}:latest"        --tag="${IMAGE_PATH}:latest" )

# Optional github environment variables.
docker_build+=( --label="github.actor=${GITHUB_ACTOR:-}" )
docker_build+=( --label="github.event=${GITHUB_EVENT_NAME:-}" )
docker_build+=( --label="github.repo=${GITHUB_REPOSITORY:-}" )
docker_build+=( --label="github.job=${GITHUB_JOB_NUMBER:-}" )
docker_build+=( --label="github.ref=${GITHUB_BASE_REF:-}" )
docker_build+=( --label="github.run=${GITHUB_RUN_ID:-}" )
docker_build+=( --label="github.sha=${GITHUB_SHA:-}" )

# finally, add the dockerfile and the build context (must be last).
docker_build+=( --file="./${1}/Dockerfile" "./${1}" )

echo && cat <<-EOF && echo && exec ${docker_build[@]}
# ========================================================================
#
# Image Name    = "${1}"
#
# Registry      = "${CONTAINER_REGISTRY:-docker.pkg.github.com}"
#
# Build Command = "${docker_build[@]}"
#
# ========================================================================
EOF
