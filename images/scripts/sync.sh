#!/usr/bin/env bash
# Rebuild github workflows from template.

set -euo pipefail # always.
cd $(dirname $0)/../..

# remove any previously generated workflows, to always keep a fresh start.
rm -f ./.github/workflows/image_*.yml

for image_name in $(bash ./images/scripts/list-images.sh); do
    # the workflow_file is where the generated workflow will be written out.
    workflow_file=".github/workflows/image_${image_name}.yml"
    # copy the template over to the new directory.
    cp ./.github/workflow-template.yml $workflow_file
    # replace (in-file) the tag "#DIRNAME#" with the image_name.
    sed -i "s/#DIRNAME#/${image_name}/g" $workflow_file
    # output some text as feedback.
    echo generated $workflow_file
done
