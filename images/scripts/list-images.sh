#!/usr/bin/env bash
set -euo pipefail
cd "$(dirname $0)/.."
find . -maxdepth 2 -type f -iname dockerfile | cut -d/ -f2- | sed -e 's/\/Dockerfile//' | sort
