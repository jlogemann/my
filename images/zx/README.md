# [`zx`] Scripts
[`zx`]: https://github.com/google/zx

## Markdown Scripts

It's possible to write scripts using markdown. Only code blocks will be executed
by zx. Try to run this file with: `zx README.md`.

### `$` shell commands

```js
await $`whoami`

```

### `__dirname` & `__filename`

```js
console.log("__dirname", chalk.yellowBright(__dirname))
console.log("__filename", chalk.yellowBright(__filename))
```

### `require()`

```js
let path = await import('path')
let { name } = require(path.join(__dirname, 'package.json'))
let logText = `Package Name is ${name}`
console.log(chalk.black.bgCyanBright(logText))
```

### `import()`
`import()` works roughly as you'd expect...
```js
let { octokit } = await import('./.common.mjs')

let octokitRepos = await octokit.rest.repos.listForOrg({ org: "octokit", type: "public" })
console.debug(octokitRepos)
```

### pipes & semicolons

totally works fine...

```js
await $`ls -1 | wc -l`

// string interpolation seems intelligent...
let foo = `hi; echo 'oops'`
await $`echo ${foo} | wc` // Vars properly quoted.

```