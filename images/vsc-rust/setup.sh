#!/usr/bin/env bash

function printHelp() {
cat <<-__HELP_TEXT

  USAGE:
    bash $(dirname $0)/$(basename $0) <COMMAND>

  COMMANDS:
    help, usage         print this help text.

__HELP_TEXT
}

function install-crate() {
    cargo install -q $@  2>&1
    rm -rf $CARGO_HOME/registry/src/*
}


function main() {
    local cmd=${1:-}
    shift 1; # strip the command name off args.
    case "$cmd" in
        -h|--help|help|usage|\?) printHelp ;;
        install-crate)
            $cmd $@ ;;
        *) echo ;;
    esac
}

set -euo pipefail; main $@