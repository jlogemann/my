#!/bin/bash
cd $(dirname $0)

ROUTES="192.168.0.0/16"
AUTHKEY="tskey-xxxxxxxxxxxxxxxxxxxxxxxx"

docker build -t tailscale .

docker run --name="tailscale-relay" -d -v /tailscale --cap-add=NET_ADMIN --network=host \
    -e ROUTES="$ROUTES" -e AUTHKEY="$AUTHKEY" tailscale:latest
