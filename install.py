#!/usr/bin/env -S python3 -B

from typing import Sequence
from config import *
import os, argparse
from pathlib import Path

SCRIPT_PATH = os.path.realpath(__file__)
ARGS = argparse.ArgumentParser(description='Installs and manages my repo')
ARGS.add_argument('features',
                  metavar='FEATURE',
                  type=str,
                  nargs='*',
                  choices=FEATURES.keys(),
                  help='Features to enable')


def main(args: argparse.Namespace):
    install(args.features)


def install(features: Sequence[str]):
    for f in features:
        install_feature(f)


def install_feature(feature_name: str):
    if feature_name not in FEATURES.keys():
        print("FEATURE \"", feature_name, "\" does not exist in FEATURES of",
              SCRIPT_PATH)
        return

    FEATURE = FEATURES[feature_name]
    src_dir = SCRIPT_DIR.joinpath(feature_name)
    dst_dir = FEATURE.get("dst_dir", CONFIG_DIR.joinpath(feature_name))

    if not Path(dst_dir).exists():
        Path(dst_dir).mkdir(parents=True)

    ignore_paths = FEATURE.get("ignore_paths",
                               ["Dockerfile", ".gitignore", ".dockerignore"])

    install_feature_dir(src_dir, dst_dir, ignore_paths)


def install_feature_dir(src_dir: Path, dst_dir: Path,
                        ignore_paths: Sequence[str]):
    for p in sorted(src_dir.rglob("*")):
        rel_path = p.relative_to(src_dir)

        # Skip any files/directories we intentionally ignore.
        if p.name in ignore_paths:
            continue

        # Determine the absolute src/dest directories.
        src_path = src_dir.joinpath(rel_path).absolute()
        dst_path = dst_dir.joinpath(rel_path).absolute()

        # If the path already exists at the destination, perhaps bail.
        if dst_path.exists():
            if dst_path.is_symlink():
                # print("unlinking", dst_path)
                dst_path.unlink()
            elif dst_path.is_file():
                print("existing file:", rel_path)
                continue
            else:
                # print("refusing to overwrite existing dir", rel_path)
                continue

        # If we've gotten this far we should attempt to create/symlink the dir/file.
        if src_path.is_dir() and dst_path.exists():
            print("directory", dst_path, "already exists")
        elif src_path.is_dir() and not dst_path.exists():
            print("creating", dst_path)
            dst_path.mkdir(mode=0o750, parents=True)
        elif src_path.is_file():
            print("linking", src_path.relative_to(SCRIPT_DIR))
            dst_path.symlink_to(src_path)
        elif src_path.is_symlink():
            print("skipping symlink", rel_path)


if __name__ == '__main__':
    main(ARGS.parse_args())
