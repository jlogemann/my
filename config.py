import os
from pathlib import Path

SCRIPT_DIR = Path(os.path.dirname(os.path.realpath(__file__)))
USER = os.environ.get("USER")
HOME_DIR = Path.home()
CONFIG_DIR = HOME_DIR.joinpath(".config")
LOCAL_DIR = HOME_DIR.joinpath(".local")
LOCAL_USER_BIN = LOCAL_DIR.joinpath("bin")

DEFAULT_FEATURES = ["dotfiles", "scripts", "neovim", "ssh", "zsh", "git", "bash"]

FEATURES = {
    "alacritty": dict(),
    "bash": dict(),
    "cheatsheets": dict(dst_dir=LOCAL_DIR.joinpath('share/navi/cheats/my_cheats')),
    "dotfiles": dict(dst_dir=HOME_DIR),
    "git": dict(),
    "neovim": dict(dst_dir=CONFIG_DIR.joinpath('nvim')),
    "ranger": dict(),
    "scripts": dict(dst_dir=LOCAL_USER_BIN),
    "ssh": dict(dst_dir=HOME_DIR.joinpath('.ssh')),
    "zsh": dict(),
}
