local execute = vim.api.nvim_command
local fn = vim.fn

-- ensure paq is installed.
local install_dir = vim.fn.stdpath("data") .. "/site/pack/paqs/opt/paq-nvim"
local install_url = "https://github.com/savq/paq-nvim.git"
if fn.empty(fn.glob(install_dir)) > 0 then
	execute(table.concat({ "!git", "clone", install_url, install_dir }, " "))
end

-- load and bind paq globally.
vim.cmd("packadd paq-nvim")
_G["paq"] = require("paq-nvim").paq
paq({ "savq/paq-nvim", opt = true }) -- paq-nvim manages itself

_G["bindmap"] = function(mode, lhs, rhs, opts)
	local options = { noremap = true }
	if opts then
		options = vim.tbl_extend("force", options, opts)
	end
	vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- paq "akinsho/nvim-bufferline.lua"
-- paq "junegunn/goyo.vim"
-- paq "junegunn/limelight.vim"
-- paq "kana/vim-textobj-function"
-- paq "RRethy/vim-illuminate"
-- paq "mattn/vim-textobj-url"
-- paq "kana/vim-textobj-user"
-- paq "AndrewRadev/sideways.vim"
-- paq "AndrewRadev/splitjoin.vim"
paq({ "shougo/deoplete-lsp" })
paq({ "shougo/deoplete.nvim", run = vim.fn["remote#host#UpdateRemotePlugins"] })
paq({ "nvim-treesitter/nvim-treesitter" })
paq({ "neovim/nvim-lspconfig" })
paq("kyazdani42/nvim-tree.lua")
paq("nvim-lua/telescope.nvim")
paq("tjdevries/express_line.nvim")
paq("bluz71/vim-nightfly-guicolors")
paq("kyazdani42/nvim-web-devicons")
paq("norcalli/nvim-colorizer.lua")
paq("tjdevries/colorbuddy.nvim")
paq("Shougo/dein.vim")
paq("Shougo/echodoc.vim")
paq("blackCauldron7/surround.nvim")
paq("hkupty/iron.nvim")
paq("jacoborus/tender.vim")
paq("jamessan/vim-gnupg")
paq("junegunn/vim-easy-align")
paq("justinmk/vim-sneak")
paq("lambdalisue/gina.vim")
paq("liuchengxu/vim-which-key")
paq("liuchengxu/vista.vim")
paq("norcalli/nvim-terminal.lua")
paq("romainl/vim-qf")
paq("tpope/vim-endwise")
paq("tpope/vim-jdaddy")
paq("tpope/vim-markdown")
paq("tpope/vim-repeat")
paq("tpope/vim-scriptease")
paq("tpope/vim-surround")
paq("tpope/vim-unimpaired")
paq("tweekmonster/helpful.vim")
paq("tyru/caw.vim")
paq("wellle/targets.vim")
paq({ "rafcamlet/nvim-luapad", cmd = "LuaPad" })
paq("chrisbra/unicode.vim")
paq("hkupty/daedalus.nvim")
paq("nvim-lua/popup.nvim")
paq("lambdalisue/suda.vim")
paq("nvim-lua/plenary.nvim")
paq("lunarmodules/Penlight")
paq("hkupty/impromptu.nvim")
paq("svermeulen/vimpeccable")
