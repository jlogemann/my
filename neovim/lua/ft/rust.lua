-- if rust is not installed, don't bother loading this configuration at all.
if !vim.fn.executable('rustc') and !vim.fn.executable('cargo') then return nil end

paq "rust-lang/rust.vim"
