-- if golang is not installed, don't bother loading this configuration at all.
if !vim.fn.executable('go') then return nil end

paq "fatih/vim-go"
