paq("sheerun/vim-polyglot")
paq({ "nvim-treesitter/nvim-treesitter" })
paq({ "neovim/nvim-lspconfig" })

require("rust")
require("go")
