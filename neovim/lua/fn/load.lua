local vimrc = _G["vimrc"]

function vimrc.fn.load_env(env_file)
	if vim.fn.filereadable(env_file) == 1 then
		local env_contents = {}
		for _, item in pairs(vim.fn.readfile(env_file)) do
			local line_content = vim.fn.split(item, "=")
			env_contents[line_content[1]] = line_content[2]
		end
		return env_contents
	else
		return {}
	end
end

function vimrc.fn.unload_lua_namespace(prefix)
	local prefix_with_dot = prefix .. "."
	for key, _ in pairs(package.loaded) do
		if key == prefix or key:sub(1, #prefix_with_dot) == prefix_with_dot then
			package.loaded[key] = nil
		end
	end
end

function vimrc.fn.secret_from_pass(secret_name)
	-- return the output of a given secret from `pass`.
	return vim.fn.system("pass show " .. secret_name)
end
