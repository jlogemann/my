local vim = assert(vim)
local vimrc = assert(vimrc)

function vim.fn.autocmd(name, opts)
	local callback = opts.callback
	local match = opts.match or { "*" }

	if vimrc.autocmd == nil then
		vimrc.autocmd = {}
	end
	vimrc.autocmd[name] = callback
	vim.cmd(
		string.format(
			"autocmd %s %s lua vimrc.autocmd['%s']()",
			table.concat((opts.events or { name }), ","),
			table.concat(match, ","),
			name
		)
	)
end

function vim.fn.augroup(name, fn)
	vim.api.nvim_command("augroup " .. name)
	vim.api.nvim_command("autocmd!")
	fn()
	vim.api.nvim_command("augroup end")
end

vimrc.fn.define_autocommand = vim.fn.autocmd
vimrc.fn.autocommand_group = vim.fn.augroup
