local vimrc = _G["vimrc"]

vim.fn.augroup("LuaVimrc", function()
	-- https://vim.fandom.com/wiki/Detect_window_creation_with_WinEnter
	vim.cmd("autocmd VimEnter * autocmd WinEnter * let w:created=1")

	-- define_autocommand("BufEnter", { callback = function()
	--   -- require'completion'.on_attach()
	-- end })

	vim.fn.autocmd("InsertEnter", {
		callback = function()
			vim.cmd("noh")
		end,
	})
	vim.fn.autocmd("InsertLeave", {
		callback = function()
			vim.o.paste = false
			vim.cmd("noh") -- disable highlight
		end,
	})

	-- ensure ShaDa file is written before exit.
	vim.fn.autocmd("VimLeave", {
		callback = function()
			vim.cmd("wshada!")
		end,
	})
	vim.fn.autocmd("VimEnter", {
		callback = function()
			vim.defer_fn(function()
				print("Welcome to my NeoVim!")
			end, 1000)
			vim.defer_fn(function()
				print(" ")
			end, 4000)
		end,
	})

	vim.fn.autocmd("RefFileTypes", {
		events = { "FileType" },
		match = { "help" },
		callback = function()
			vimrc.util.buf.disable_extras(0, 0)
		end,
	})

	-- automatically select readonly when swap exists.
	vim.fn.autocmd("SwapExists", {
		callback = function()
			vim.v.swapchoice = "o"
		end,
		match = { "* nested" },
	})
	vim.fn.autocmd("TextYankPost", {
		callback = function()
			require("vim.highlight").on_yank()
		end,
	})
end)
