-- vim: ft=lua
--
-- Initialize Package Manager
local fn = vim.fn

-- ensure paq is installed.
if vim.fn.empty(vim.fn.glob(install_dir)) > 0 then
	local paq_install_dir = vim.fn.stdpath("data") .. "/site/pack/paqs/opt/paq-nvim"
	vim.api.nvim_command(table.concat({
		"silent!",
		"!test",
		"-d",
		paq_install_dir,
		"||",
		"git",
		"clone",
		"https://github.com/savq/paq-nvim.git",
		paq_install_dir,
	}, " "))
end

-- load and bind paq globally.
vim.cmd("packadd paq-nvim")
_G["paq"] = require("paq-nvim").paq
paq({ "savq/paq-nvim", opt = true }) -- paq-nvim manages itself

-- detect the operating system.
if vim.fn.has("mac") == 1 then
	local os_family = "macOS"
elseif vim.fn.has("unix") == 1 then
	local os_family = "Linux"
elseif vim.fn.has("win32") == 1 then
	local os_family = "Windows"
else
	local os_family = "Unsupported"
end

-- define the default vimrc global variable if its not already defined.
if _G["vimrc"] == nil then
	_G["vimrc"] = {
		os_family = os_family,
		fn = {},
	}
end

function vimrc.set_globals()
	-- Initialize/Setup Global Libraries.
	require("surround").setup({})
	require("terminal").setup({})

	local vimp = require("vimp")
	_G["vimp"] = vimp

	local impromptu = require("impromptu")
	_G["impromptu"] = impromptu

	local plenary = require("plenary")
	plenary.Job = require("plenary.job")
	plenary.context_manager = require("plenary.context_manager")
	plenary.filetype = require("plenary.filetype")
	plenary.popup = require("plenary.popup")
	plenary.scandir = require("plenary.scandir")
	plenary.strings = require("plenary.strings")
	plenary.window = require("plenary.window")
	_G["plenary"] = plenary

	local pl = {}
	pl.List = require("pl.List")
	pl.lexer = require("pl.lexer")
	pl.pretty = require("pl.pretty")
	pl.seq = require("pl.seq")
	pl.template = require("pl.template")
	pl.stringx = require("pl.stringx")
	pl.utils = require("pl.utils")
	_G["pl"] = pl
end

function vimrc.load()
	-- load core vimrc functions
	require("fn") -- (keep this first)
	-- load vimrc options
	require("options") -- (keep this second)
	-- load vimrc third-party packages
	require("packages") -- (keep this third)
	vimrc.set_globals()

	-- load vimrc utilities & helpers
	require("util") -- (keep this forth)

	-- load main vimrc configurations.
	require("autocommands")
	require("colors")
	require("commands")
	require("file_tree")
	require("completion")
	require("find")
	require("mappings")
	require("statusline")
end

function vimrc.reload()
	print("reloading entire vimrc namespace")
	vimrc.fn.unload_lua_namespace("vimrc")
	vimrc.setup()
end

function vimrc.setup()
	vimrc.load()

	-- set opts in neovim (buffer, window, global, terminal, env vars, etc).
	if vimrc and vimrc.opts then
		for k, v in pairs(vimrc.opts.g) do
			vim.g[k] = v
		end
		for k, v in pairs(vimrc.opts.o) do
			vim.o[k] = v
		end
		for k, v in pairs(vimrc.opts.t) do
			vim.t[k] = v
		end
		for k, v in pairs(vimrc.opts.env) do
			vim.env[k] = v
		end
	end

	if vimrc and vimrc.fn.init_commands then
		vimrc.fn.init_commands()
	end
	if vimrc and vimrc.fn.setup_completion then
		vimrc.fn.setup_completion()
	end
	if vimrc and vimrc.fn.setup_statusline then
		vimrc.fn.setup_statusline()
	end
	if vimrc and vimrc.fn.setup_commands then
		vimrc.fn.setup_commands()
	end
end

-- finally, execute the configuration setup.
vimrc.setup()
