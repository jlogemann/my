# -----------------------------------------------------------------------------------
#         888888888888             88                                   
#                  ,88             88                                   
#                ,88"              88                                   
#              ,88"     ,adPPYba,  88,dPPYba,   8b,dPPYba,   ,adPPYba,  
#            ,88"       I8[    ""  88P'    "8a  88P'   "Y8  a8"     ""  
#          ,88"          `"Y8ba,   88       88  88          8b          
#         88"           aa    ]8I  88       88  88          "8a,   ,aa  
#         888888888888  `"YbbdP"'  88       88  88           `"Ybbd8"'  
# -----------------------------------------------------------------------------------
# Author: Jake Logemann (https://github.com/JakeLogemann) 
 
[[ -n "$zsh_dir" ]] || readonly zsh_dir="$XDG_CONFIG_DIR/zsh"
fpath=( "$zsh_dir/completions" "$zsh_dir/functions" $fpath )
path+=( "$HOME/.fzf/bin" "$HOME/.cargo/bin" "$HOME/.rbenv/bin" "$HOME/.rbenv/shims" "/usr/local/bin")

# Helper Functions                                                  {{{1
#=======================================================================
# Helper functions (for early-use)
function has_bin(){ for var in "$@"; do which $var 2>/dev/null >&2; done; }
function source_if_exists(){ for var in "$@"; do test ! -r "$var" || source "$var"; done; }
function maybe_run_bin(){ if has_bin "$1"; then eval "$@"; fi; }
function maybe_eval_bin(){ if has_bin "$1"; then eval "$($*)"; fi; }
function scp_to_same(){ scp -rp "$1" "$2:$1" ;}
function dotfiles::run-navi() {  BUFFER=" navi"; zle accept-line; }
function dotfiles::skim-files-in-directory() {  BUFFER=" sk --ansi -i -c 'grep -rI --color=always --line-number \"{}\" .'"; zle accept-line; }
function dotfiles::skim-directory() {  BUFFER=' vim -p $(find . -type f | sk -m)'; zle accept-line; }

# List all defined options, in a more pretty way.
function list-fpath(){ echo $fpath | tr " " "\n" | nl ;}
function list-path(){ echo $path | tr " " "\n" | nl ;}
function list-hosts(){ cat /etc/hosts |column -t ;}
function list-users(){ cat /etc/passwd |column -ts: | sort -nk3 ;}
function list-keybinds(){ bindkey |grep -v "magic-space"  |tr -d "\""| column -t ;}

function background() { # starts one or multiple args as programs in background
  for ((i=2;i<=$#;i++)); do ${@[1]} ${@[$i]} &> /dev/null &; done
}

function uuid() { # Usage: uuid
  C="89ab"
  for ((N=0;N<16;++N)); do
      B="$((RANDOM%256))"
      case "$N" in
          6)  printf '4%x' "$((B%16))" ;;
          8)  printf '%c%x' "${C:$RANDOM%${#C}:1}" "$((B%16))" ;;
          3|5|7|9) printf '%02x-' "$B" ;;
          *) printf '%02x' "$B" ;;
      esac
  done
  printf '\n'
}


typeset -a baliases; baliases=(); # blank aliases
typeset -a ialiases; ialiases=(); # ignored aliases
function balias() { alias $@; args="$@"; args=${args%%\=*}; baliases+=(${args##* }); }
function ialias() { alias $@; args="$@"; args=${args%%\=*}; ialiases+=(${args##* }); }
function expand-alias-space() {
  [[ $LBUFFER =~ "\<(${(j:|:)baliases})\$" ]]; insertBlank=$?
  if [[ ! $LBUFFER =~ "\<(${(j:|:)ialiases})\$" ]]; then zle _expand_alias; fi
  zle self-insert
  if [[ "$insertBlank" = "0" ]]; then zle backward-delete-char; fi
}


# Third-Party/Vendor Scripts                                        {{{1
#=======================================================================
maybe_eval_bin starship init zsh
maybe_eval_bin zoxide init --cmd="goto" zsh
maybe_eval_bin direnv hook zsh
maybe_eval_bin rbenv init -
maybe_eval_bin pyenv init -
source_if_exists "$HOME/.profile"
source_if_exists "$HOME/.rbenv/completions/rbenv.zsh"

if [[ -n "${SSH_CONNECTION}" && "$TERM" == "alacritty" ]]; then export TERM=xterm-256color; fi

# Standard ZSH Modules                                              {{{1
#=======================================================================
# load applicable zsh modules
zmodload \
  "zsh/attr" \
  "zsh/cap" \
  "zsh/clone" \
  "zsh/complete" \
  "zsh/complist" \
  "zsh/computil" \
  "zsh/curses" \
  "zsh/langinfo" \
  "zsh/mathfunc" \
  "zsh/parameter" \
  "zsh/regex" \
  "zsh/sched" \
  "zsh/system" \
  "zsh/termcap" \
  "zsh/terminfo" \
  "zsh/zle" \
  "zsh/zleparameter" \
  "zsh/zpty" \
  "zsh/zselect" \
  "zsh/zutil"

autoload -Uz promptinit colors; promptinit; colors;
autoload -Uz compinit bashcompinit && compinit && bashcompinit

# Aliases                                                           {{{1
#=======================================================================
if has_bin lsd; then
  ialias l1='lsd -1'
  ialias l='lsd'
  ialias ll='lsd -Alh --date relative --size short --no-symlink'
  ialias ls='lsd -A'
  ialias lss='lsd -Alh --date relative --size short --no-symlink --sizesort'
  ialias lst='lsd -Alh --date relative --size short --no-symlink --timesort'
else
  ialias l1='ls -1'
  ialias l='ls'
  ialias ll='ls -Alh'
  ialias ls='ls -A'
  ialias lss='ls -Alh'
  ialias lst='ls -Alh'
fi

for e in "e" "nano" "pico" "vi" "edit"; do ialias $e="$EDITOR"; done
while IFS= read -r line; do eval "ialias $line"; done < "$HOME/.config/aliases.ini"

# Keybindings                                                       {{{1
#=======================================================================
source_if_exists "$HOME/.nix-profile/etc/profile.d/nix.sh"
source_if_exists "$HOME/.config/zsh/options.zsh"
source_if_exists "$HOME/.config/zsh/completion.zsh"
source_if_exists "$HOME/.config/zsh/keybinds.zsh"
source_if_exists "$HOME/.config/zsh/plugins/skim.zsh"
source_if_exists "$HOME/.zshrc.local"
